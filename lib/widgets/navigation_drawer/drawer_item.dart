import 'package:flutter/material.dart';
import 'package:flutter_web/constants/app_colors.dart';
import 'package:flutter_web/widgets/navigation_bar/narbar_item.dart';

class DrawerItem extends StatelessWidget {
  final String title;
  final IconData icon;
  DrawerItem(this.title, this.icon);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 30, top: 60),
      child: Row(
        children: [
          Icon(icon),
          SizedBox(
            width: 30,
          ),
          NavBarItem(title)
        ],
      ),
    );
  }
}
