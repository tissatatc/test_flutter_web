import 'package:flutter/material.dart';

const Color primaryColor = Colors.pinkAccent;
const Color secondaryColor = Colors.yellowAccent;
const Color backgroundColor = Colors.blueGrey;
